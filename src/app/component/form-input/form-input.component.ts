import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent {

  @Input() data: any
  @Input() placeholder: string = ""

  @Output() dataChange = new EventEmitter<any>()

  change(val: any): void {
    this.dataChange.emit(val.target.value)
  }

}
